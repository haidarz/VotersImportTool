﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using VotersImportTool.Constants;
using VotersImportTool.Models;
using VotersImportTool.Resources;


namespace VotersImportTool.Helpers
{
    public class DataHelper
    {
        public static DataTable GetDataTableFromCsv(string strFileName)
        {
            string defaultQuery = $"Select * from [{System.IO.Path.GetFileName(strFileName)}]";
            return GetDataTableFromCsv(strFileName, defaultQuery);
        }

        public static DataTable GetDataTableFromCsv(string strFileName, string query)
        {
            try
            {
                string filePath = System.IO.Path.GetDirectoryName(strFileName);
                string connectionString = $@"Provider=Microsoft.Jet.OLEDB.4.0; Data Source={filePath}; Extended Properties = ""Text; CharacterSet=1256;HDR=NO;FMT=Delimited""";
                OleDbConnection conn = new OleDbConnection(connectionString);
                conn.Open();
                string strQuery = query;
                OleDbDataAdapter da = new OleDbDataAdapter(strQuery, conn);
                DataSet ds = new DataSet();
                da.Fill(ds, System.IO.Path.GetFileName(strFileName));
                conn.Close();
                DataTable table = ds.Tables[0];
                return table;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            return new DataTable();
        }

        public static DataSet GetDataSetFromCsvs(string[] fileNames)
        {
            DataSet dataSet = new DataSet();
            try
            {
                foreach (string fileName in fileNames)
                {
                    DataTable table = GetDataTableFromCsv(fileName);
                    dataSet.Tables.Add(table.Copy());
                    table.Clear();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
            return dataSet;
        }


        public static void SeedElectoralInfoFromDataSet(DataSet dataSet)
        {
            try
            {
                List<Province> provinces = new List<Province>();
                List<Gender> genders = new List<Gender>()
                {
                    new Gender()
                    {
                        Name = Columns.Male
                    },
                    new Gender()
                    {
                        Name = Columns.Female
                    }
                };
                List<Judiciary> judiciaries = new List<Judiciary>();
                List<Village> villages = new List<Village>();
                List<ListReligion> listReligions = new List<ListReligion>();
                List<Voter> voters = new List<Voter>();

                // Shared Electoral
                Province province = null;
                Village village = null;
                Gender gender = null;
                Judiciary judiciary = null;
                ListReligion listReligion = null;

                foreach (DataTable table in dataSet.Tables)
                {
                    for (int i = 0; i < table.Rows.Count;)
                    {
                        // Current Row
                        DataRow row = table.Rows[i];

                        // Test the first column value to fill electoral info
                        DataColumn column = table.Columns[ElectoralIndexes.Province];
                        object item = row[column];
                        if (null != item && item.ToString() == Columns.Province)
                        {
                            i = i + 1;
                            DataRow electoralInfoRow = table.Rows[i];

                            //Province
                            DataColumn provinceDataColumn = table.Columns[ElectoralIndexes.Province];
                            string provinceName = electoralInfoRow[provinceDataColumn].ToString();
                            province = provinces.FirstOrDefault(p => p.Name == provinceName);
                            if (null == province)
                            {
                                province = new Province()
                                {
                                    Name = provinceName
                                };
                                provinces.Add(province);
                            }

                            // Judiciary
                            DataColumn judiciaryDataColumn = table.Columns[ElectoralIndexes.Judiciary];
                            string judiciaryName = electoralInfoRow[judiciaryDataColumn].ToString();
                            judiciary = judiciaries.FirstOrDefault(j => j.Name == judiciaryName && j.Province.Name == provinceName);
                            if (null == judiciary)
                            {
                                judiciary = new Judiciary()
                                {
                                    Name = judiciaryName,
                                    Province = province
                                };
                                judiciaries.Add(judiciary);
                            }

                            // Village
                            DataColumn villageDataColumn = table.Columns[ElectoralIndexes.Village];
                            string villageName = electoralInfoRow[villageDataColumn].ToString();
                            village = villages.FirstOrDefault(v => v.Name == villageName && v.Judiciary.Name == judiciaryName);
                            if (null == village)
                            {
                                village = new Village()
                                {
                                    Name = villageName,
                                    Judiciary = judiciary
                                };
                                villages.Add(village);
                            }

                            // List Religion
                            DataColumn listReligionDataColumn = table.Columns[ElectoralIndexes.ReligionList];
                            string listReligionName = electoralInfoRow[listReligionDataColumn].ToString();
                            listReligion = listReligions.FirstOrDefault(l => l.Name == listReligionName);
                            if (null == listReligion)
                            {
                                listReligion = new ListReligion()
                                {
                                    Name = listReligionName
                                };
                                listReligions.Add(listReligion);
                            }

                            // Jump over the voter headers
                            i = i + 2;
                            continue;
                        }
                        i++;
                    }
                }

                using (VotersImportToolContext context = new VotersImportToolContext())
                {
                    context.Provinces.AddRange(provinces);
                    context.Judiciaries.AddRange(judiciaries);
                    context.Villages.AddRange(villages);
                    context.ListReligions.AddRange(listReligions);
                    context.Genders.AddRange(genders);
                    context.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        public static void SeedVotersFromDataSet(DataSet dataSet)
        {
            try
            {
                using (var context = new VotersImportToolContext())
                {
                    List<Province> provinces = context.Provinces.ToList();
                    List<Gender> genders = context.Genders.ToList();
                    List<Judiciary> judiciaries = context.Judiciaries.ToList();
                    List<Village> villages = context.Villages.ToList();
                    List<ListReligion> listReligions = context.ListReligions.ToList();
                    List<Voter> voters = new List<Voter>();

                    // Shared Electoral
                    Province province = null;
                    Village village = null;
                    Gender gender = null;
                    Judiciary judiciary = null;
                    ListReligion listReligion = null;

                    foreach (DataTable table in dataSet.Tables)
                    {
                        for (int i = 0; i < table.Rows.Count;)
                        {
                            // Current Row
                            DataRow row = table.Rows[i];

                            // Test the first column value to fill electoral info
                            DataColumn column = table.Columns[ElectoralIndexes.Province];
                            object item = row[column];
                            if (null != item && item.ToString() == Columns.Province)
                            {
                                i = i + 1;
                                DataRow electoralInfoRow = table.Rows[i];

                                //Province
                                DataColumn provinceDataColumn = table.Columns[ElectoralIndexes.Province];
                                string provinceName = electoralInfoRow[provinceDataColumn].ToString();
                                province = provinces.FirstOrDefault(p => p.Name == provinceName);

                                // Judiciary
                                DataColumn judiciaryDataColumn = table.Columns[ElectoralIndexes.Judiciary];
                                string judiciaryName = electoralInfoRow[judiciaryDataColumn].ToString();
                                judiciary = judiciaries.FirstOrDefault(j => j.Name == judiciaryName && j.Province.Name == provinceName);

                                // Village
                                DataColumn villageDataColumn = table.Columns[ElectoralIndexes.Village];
                                string villageName = electoralInfoRow[villageDataColumn].ToString();
                                village = villages.FirstOrDefault(v => v.Name == villageName && v.Judiciary.Name == judiciaryName);

                                // List Religion
                                DataColumn listReligionDataColumn = table.Columns[ElectoralIndexes.ReligionList];
                                string listReligionName = electoralInfoRow[listReligionDataColumn].ToString();
                                listReligion = listReligions.FirstOrDefault(l => l.Name == listReligionName);

                                // Gender
                                DataColumn genderDataColumn = table.Columns[ElectoralIndexes.Gender];
                                string genderName = electoralInfoRow[genderDataColumn].ToString();
                                gender = genders.FirstOrDefault(g => g.Name == Columns.Male);

                                // Jump over the voter headers
                                i = i + 2;
                                continue;
                            }
                            // iterate to the next row

                            // Fill the voter information
                            Voter voter = new Voter();
                            string firstName = row[table.Columns[VoterIndexes.FirstName]].ToString();
                            string middleName = row[table.Columns[VoterIndexes.MiddleName]].ToString();
                            string lastName = row[table.Columns[VoterIndexes.LastName]].ToString();
                            string motherName = row[table.Columns[VoterIndexes.MotherName]].ToString();
                            object birthDateObject = row[table.Columns[VoterIndexes.BirthDate]];
                            // DateTime? birthDate = DataCleaningUtility.ParseStringToDateTime(birthDateString);
                            string registrationNumberString = row[table.Columns[VoterIndexes.RegistrationNumber]].ToString();
                            int registrationNumber = DataCleaningUtility.ParseStringToInteger(registrationNumberString);
                            voter.FirstName = firstName;
                            voter.FatherName = middleName;
                            voter.LastName = lastName;
                            voter.MotherName = motherName;
                            if (birthDateObject is DateTime)
                                voter.BirthDate = (DateTime)birthDateObject;
                            voter.RegistrationNumber = registrationNumber;
                            voter.Village = village;
                            voter.Gender = gender;
                            voters.Add(voter);

                            i++;
                        }
                    }

                    context.Configuration.AutoDetectChangesEnabled = false;
                    context.Voters.AddRange(voters);
                    context.SaveChanges();
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        public static void FillProvince(string[] fileNames)
        {
            DataSet dataSet = GetDataSetFromCsvs(fileNames);
            SeedElectoralInfoFromDataSet(dataSet);
            SeedVotersFromDataSet(dataSet);
        }

    }
}

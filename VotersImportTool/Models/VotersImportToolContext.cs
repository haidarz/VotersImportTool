﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VotersImportTool.Models
{
    class VotersImportToolContext : DbContext
    {
        public VotersImportToolContext() : base("DefaultConnection")
        {

        }
        public DbSet<Voter> Voters { get; set; }
        public DbSet<Judiciary> Judiciaries { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<Religion> Religions { get; set; }
        public DbSet<ListReligion> ListReligions { get; set; }
        public DbSet<Village> Villages { get; set; }
        public DbSet<Gender> Genders { get; set; }
    }
}

﻿using System.Collections.Generic;

namespace VotersImportTool.Models
{
    public class Judiciary
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Province Province { get; set; }
        public List<Village> Villages { get; set; }
    }
}